use bevy::pbr::AmbientLight;
use bevy::prelude::*;
use bevy::render::camera::{Camera, PerspectiveProjection};
use bevy::render::mesh::{Indices, VertexAttributeValues};
use bevy_rapier3d::prelude::*;
// use bevy_inspector_egui::{InspectorPlugin, Inspectable, WorldInspectorPlugin};

use bevy::render::pass::ClearColor;
use bevy_flycam::{MovementSettings, PlayerPlugin};
use crate::grouper::Grouper;
use crate::killfeed::{Killfeed, KillfeedItem};
use crate::load_level::generate_level;
use crate::names::NAMES;
use crate::ranking::RankingPlugin;
use crate::slow_rotate::ZeppelinPlugin;
use crate::sound::click_sound_system;
use crate::trigger_boxes::{handle_finish_box, WinnersList};
use crate::main_menu::MainMenuPlugin;

mod grouper;
mod load_level;
mod killfeed;
mod names;
mod cube_bundle;
mod generate_geometry;
mod profiles;
mod slow_rotate;
mod trigger_boxes;
mod ranking;
mod sound;
mod main_menu;
mod game_state;

pub struct BallLabel {
    label: String,
    finished: bool,
}

struct MeshCollider {
    mesh: Handle<Mesh>,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
enum AppState {
    MainMenu,
    InGame,
}


fn main() {
    App::build()
        .insert_resource(
            WindowDescriptor {
                title: "Rusty Marbles".to_string(),
                width: 1800.,
                height: 900.,
                ..Default::default()
            }
        )
        .insert_resource(ClearColor(Color::rgb(
            0x29 as f32 / 255.0,
            0x29 as f32 / 255.0,
            0x3F as f32 / 255.0,
        )))
        .add_state(AppState::MainMenu)
        .insert_resource(
            AmbientLight {
                color: Color::rgb(
                    0x29 as f32 / 255.0,
                    0x29 as f32 / 255.0,
                    0x3f as f32 / 255.0),
                brightness: 0.2,
            }
        )
        .add_plugins(DefaultPlugins)
        .add_system_set(
            SystemSet::on_update(
                AppState::InGame
            ).with_system(update_text_system.system())
                .with_system(
                    fix_collision_models.system())
                .with_system(
                    handle_finish_box.system())
                .with_system(
                    start_on_space.system()
                )
                .with_system(
                    delete_balls_below_level.system()
                )
        )
        .add_system_set(
            SystemSet::on_enter(
                AppState::InGame
            ).with_system(
                init_level.system()
            ).with_system(
                generate_level.system()
            ).with_system(
                click_sound_system.system()
            ).with_system(
                spawn_balls_system.system()
            )
        )
        .add_plugin(PlayerPlugin)
        .add_plugin(
            RapierPhysicsPlugin::<NoUserData>::default())
        .insert_resource(
            RapierConfiguration {
                physics_pipeline_active: false,
                ..Default::default()
            }
        )
        .insert_resource({
            MovementSettings {
                speed: 12.0,
                ..Default::default()
            }
        })
        .add_startup_system(setup_graphics.system())
        .add_plugin(killfeed::KillfeedPlugin)
        .add_plugin(ZeppelinPlugin {})
        .add_plugin(RankingPlugin {})
        .add_plugin(MainMenuPlugin {})
        .insert_resource(
            WinnersList {
                ranking: Vec::new()
            }
        )
        .run();
}

struct FollowBallsText;

fn setup_graphics(mut commands: Commands) {
    commands.spawn().insert(
        AmbientLight {
            color: Color::WHITE,
            brightness: 0.4,
        }
    );

    commands.spawn_bundle(UiCameraBundle::default());
}

fn spawn_balls_system(mut commands: Commands, asset_server: Res<AssetServer>) {
    let font = asset_server.load("fonts/roboto.ttf");

    for _ in 0..100 {
        commands.spawn_bundle(
            TextBundle {
                style: Style {
                    align_self: AlignSelf::FlexEnd,
                    position_type: PositionType::Absolute,
                    position: Rect {
                        bottom: Val::Px(5.0),
                        right: Val::Px(5.0),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                text: Text::with_section(
                    "Unnamed ball",
                    TextStyle {
                        font: font.clone(),
                        font_size: 35.0,
                        color: Color::WHITE,
                    },
                    // Note: You can use `Default::default()` in place of the `TextAlignment`
                    TextAlignment {
                        horizontal: HorizontalAlign::Center,
                        ..Default::default()
                    },
                ),
                ..Default::default()
            }
        ).insert(
            FollowBallsText {}
        );
    }
}

fn init_level(mut commands: Commands,
              asset_server: Res<AssetServer>,
              mut meshes: ResMut<Assets<Mesh>>,
              mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let num = 167.0_f32.sqrt().ceil() as usize;
    let rad = 0.12;
    let track_width = 4.5;
    let shift = track_width / (num as f32 + 2.0);

    let mut body_entities = Vec::new();

    //let texture: Handle<Texture> = asset_server.load("textures/raw/albedo.png");
    let materials = [
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.8, 0.1, 0.1),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.1, 0.8, 0.1),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.1, 0.1, 0.8),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.8, 0.8, 0.1),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.1, 0.8, 0.8),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.4, 0.05, 0.05),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.05, 0.4, 0.05),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.01, 0.04, 0.4),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.6, 0.4, 0.1),
            ..Default::default()
        }),
        materials.add(StandardMaterial {
            base_color: Color::rgb(0.1, 0.6, 0.4),
            ..Default::default()
        }),
    ];

    for k in 0..num {
        for i in 0..num {
            let fk = k as f32;
            let fi = i as f32;

            let rigid_body = RigidBodyBundle {
                body_type: RigidBodyType::Dynamic,
                position: [
                    -track_width / 2.0 + fk * shift + fi * 0.001,
                    3.1 + ((fk + fi) % 4 as f32) * 0.2 - fi * 0.3,
                    fi * shift].into(),
                ccd: RigidBodyCcd { ccd_enabled: true, ccd_thickness: rad * 1.3, ..Default::default() },
                velocity: RigidBodyVelocity {
                    linvel: [
                        -0.15 * (fk - (num as f32) / 2.0), 1.6 * ((k as i32 - i as i32) % 3) as f32 + 1.3, 0.6 + 0.1 * fi].into(),
                    ..Default::default()
                },
                ..Default::default()
            };

            let collider = if (i + k) % 12 != 0 {
                ColliderBundle {
                    shape: ColliderShape::ball(rad),
                    ..Default::default()
                }
            } else {
                ColliderBundle {
                    shape: ColliderShape::cuboid(rad / 2.0, rad / 2.0, rad / 2.0),
                    ..Default::default()
                }
            };

            let child_entity = commands
                .spawn_bundle(collider)
                .insert_bundle(rigid_body)
                .insert_bundle(
                    PbrBundle {
                        mesh: meshes.add(Mesh::from(bevy::prelude::shape::Icosphere { radius: rad, subdivisions: 3 })),
                        material: materials[(i + k * num) % materials.len()].clone(),
                        ..Default::default()
                    }
                )
                .insert(ColliderPositionSync::Discrete)
                .insert(BallLabel {
                    label: NAMES[(i * num + k) % NAMES.len()].to_string(),
                    finished: false,
                })
                .id();

            body_entities.push(child_entity);
        }
    }
}

fn update_text_system(mut query: Query<(&mut Text, &mut Style), With<FollowBallsText>>,
                      balls: Query<(&ColliderPosition, &BallLabel)>,
                      camera: Query<(&GlobalTransform, &Camera), With<PerspectiveProjection>>,
                      windows: Res<Windows>, ) {
    let cam = camera.single().unwrap();

    let screen = windows.get_primary().unwrap();
    let screen_size = (screen.width(), screen.height());

    let mut balls_iter = balls.iter();
    for (mut text, mut text_transform) in query.iter_mut() {
        let mut valid = false;
        while !valid {
            match balls_iter.next() {
                Some((ball_position, ball_label)) => {
                    match cam.1.world_to_screen(&windows, cam.0, Vec3::from(ball_position.translation)) {
                        Some(x) => {
                            if x.x >= 0.0 && x.y >= 0.0 && x.x <= screen_size.0 && x.y <= screen_size.1 {
                                let distance = (cam.0.translation - ball_position.translation.into()).length_squared();
                                if distance < 240.0 {
                                    text_transform.position = Rect {
                                        left: Val::Px(x.x),
                                        bottom: Val::Px(x.y),
                                        ..Default::default()
                                    };
                                    text.sections[0].style.color = Color::rgba_linear(
                                        1.0,
                                        1.0,
                                        1.0,
                                        (1.0 - distance / 240.0).clamp(0.0, 1.0),
                                    );
                                    text.sections[0].value = ball_label.label.clone();
                                    valid = true;
                                }
                            }
                        }
                        None => {}
                    }
                }
                None => {
                    text_transform.position.left = Val::Px(-100.0);
                    text_transform.position.bottom = Val::Px(-100.0);
                    break;
                }
            }
        }
    }
}

fn fix_collision_models(
    mut ev_asset: EventReader<AssetEvent<Mesh>>,
    assets: ResMut<Assets<Mesh>>,
    mut meshes: Query<(&MeshCollider, &mut ColliderShape)>,
) {
    for ev in ev_asset.iter() {
        match ev {
            AssetEvent::Created { handle } | AssetEvent::Modified { handle } => {
                let mut collider: Option<SharedShape> = None;

                let filtered_meshes = meshes.iter_mut().filter(|x| x.0.mesh.id == handle.id);
                for (_, mut shape) in filtered_meshes {
                    if collider.is_none() {
                        let mesh_collider = assets.get(handle.clone()).unwrap();
                        let mesh_collider_shape = ColliderShape::trimesh(
                            match mesh_collider.attribute(Mesh::ATTRIBUTE_POSITION) {
                                Some(VertexAttributeValues::Float3(vec)) => {
                                    vec.iter().map(
                                        |x| nalgebra::Point3::new(x[0], x[1], x[2])
                                    ).collect()
                                }
                                _ => {
                                    panic!("Invalid mesh type")
                                }
                            },
                            Grouper::<_, 3>::new(
                                match mesh_collider.indices().unwrap() {
                                    Indices::U32(x) => { x.iter().map(|&x| x) }
                                    Indices::U16(_) => { panic!("Error"); }//x.iter().map::<u32, u32>(|&e|->u32 {e as u32})
                                }).collect(),
                        );
                        collider = Some(mesh_collider_shape)
                    }
                    *shape = collider.clone().unwrap();
                }
            }
            AssetEvent::Removed { handle: _ } => ()
        }
    }
}

fn delete_balls_below_level(
    mut commands: Commands, query: Query<(Entity, &BallLabel, &GlobalTransform)>,
    mut killfeed: ResMut<Killfeed>,
    time: Res<Time>,
) {
    for (entity, label, global_transform) in query.iter() {
        if global_transform.translation.y < -100.0 {
            commands.entity(entity).despawn();
            killfeed.feed.push(
                KillfeedItem {
                    name: label.label.clone(),
                    time: time.seconds_since_startup(),
                }
            )
        }
    }
}

fn start_on_space(
    mut config: ResMut<RapierConfiguration>,
    keys: Res<Input<KeyCode>>,
) {
    if keys.just_pressed(KeyCode::Q) || keys.just_pressed(KeyCode::E) {
        config.physics_pipeline_active ^= true;
    }
}