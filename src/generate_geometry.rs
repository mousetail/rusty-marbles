use std::fs::File;
use std::io::Write;
use bevy::prelude::*;
use bevy::render::mesh::Indices;
use bevy::render::pipeline::PrimitiveTopology;
use json::*;

const TEXTURE_VERTICAL_SCALE: f32 = 0.128;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct ProfilePoint {
    pub position: Vec2,
    pub uvs: (f32, f32),
}

#[derive(Clone, Debug, Default)]
pub struct TrackProfileGeometry {
    pub points: Vec<ProfilePoint>,
    pub cap_shape: Vec<(u32, u32, u32)>,
}

#[derive(Clone, Debug, Default)]
pub struct TrackGeometry {
    pub start_profile: TrackProfileGeometry,
    pub end_profile: TrackProfileGeometry,
    pub start_x_angle: f32,
    pub end_offset: Vec3,
    pub end_x_angle: f32,
    pub end_y_angle: f32,
    pub start_cap: bool,
    pub end_cap: bool,
}


fn bezier(x1: f32, x2: f32, x3: f32, x4: f32, t: f32) -> f32 {
    let t_squared = t * t;
    let t_cubed = t * t * t;

    let t_inverse = 1.0 - t;
    let t_inverse_squared = t_inverse * t_inverse;
    let t_inverse_cubed = t_inverse * t_inverse * t_inverse;

    return x1 * t_inverse_cubed +
        x2 * 3.0 * t_inverse_squared * t +
        x3 * 3.0 * t_inverse * t_squared +
        x4 * t_cubed;
}

fn bezier_tangent(
    x1: f32, x2: f32, x3: f32, x4: f32, t: f32,
) -> f32 {
    3.0 * (1.0 - t) * (1.0 - t) * (x2 - x1) +
        6.0 * (1.0 - t) * t * (x3 - x2) +
        3.0 * t * t * (x4 - x3)
}

pub fn bezier_3d(x1: Vec3, x2: Vec3, x3: Vec3, x4: Vec3, t: f32) -> Vec3 {
    [
        bezier(x1.x, x2.x, x3.x, x4.x, t),
        bezier(x1.y, x2.y, x3.y, x4.y, t),
        bezier(x1.z, x2.z, x3.z, x4.z, t),
    ].into()
}

pub fn bezier_tangent_3d(
    x1: Vec3, x2: Vec3, x3: Vec3, x4: Vec3, t: f32,
) -> Vec3 {
    [
        bezier_tangent(x1.x, x2.x, x3.x, x4.x, t),
        bezier_tangent(x1.y, x2.y, x3.y, x4.y, t),
        bezier_tangent(x1.z, x2.z, x3.z, x4.z, t),
    ].into()
}

pub fn get_bezier_points(x1: Vec3, angle1: Quat, x2: Vec3, angle2: Quat) -> (Vec3, Vec3, Vec3, Vec3) {
    let length = x1.distance(x2) / 3.0;
    (
        x1,
        x1 + angle1.mul_vec3(Vec3::new(0.0, 0.0, length)),
        x2 - angle2.mul_vec3(Vec3::new(0.0, 0.0, length)),
        x2
    )
}

pub fn create_track_mesh(definition: TrackGeometry, resolution: usize, hash: u64,
) -> Mesh {
    if definition.end_profile.points.len() != definition.start_profile.points.len() {
        panic!("Invalid track definition, points must be the same on both sides");
    }

    let profile_length = definition.end_profile.points.len();

    let start_quat = Quat::from_rotation_x(definition.start_x_angle);
    //let vector_start = start_quat.mul_vec3([0.0, 0.0, 1.0].into());
    let end_quat = Quat::from_rotation_y(definition.end_y_angle) * Quat::from_rotation_x(definition.end_x_angle);
    //let end_vector = end_quat.mul_vec3([0.0, 0.0, 1.0].into());

    let mut raw_vertexes: Vec<Vec3> = Vec::with_capacity(resolution * profile_length);
    let mut raw_tangents: Vec<Vec3> = Vec::with_capacity(resolution * profile_length);
    let mut raw_uvs: Vec<(f32, f32)> = Vec::with_capacity(resolution * profile_length);

    let mut vertexes: Vec<Vec3> = Vec::with_capacity(resolution * profile_length * 2);
    let mut normals: Vec<Vec3> = Vec::with_capacity(resolution * profile_length * 2);
    let mut tangents: Vec<Vec3> = Vec::with_capacity(resolution * profile_length * 2);
    let mut uvs: Vec<Vec2> = Vec::with_capacity(resolution * profile_length * 2);
    let mut indicies: Vec<u32> = Vec::with_capacity(2 * resolution * profile_length * 3);

    let mut total_length: f32 = 0.0;

    for (point1, point2) in definition.start_profile.points.iter().zip(
        definition.end_profile.points.iter()
    ) {
        // let length = start_quat.mul_vec3(point1.position.extend(0.0)).distance(
        //     end_quat.mul_vec3(point2.position.extend(0.0)) + definition.end_offset
        // ) / 3.0;

        let points = get_bezier_points(
            start_quat.mul_vec3(point1.position.extend(0.0)),
            start_quat,
            end_quat.mul_vec3(point2.position.extend(0.0)) + definition.end_offset,
            end_quat,
        );
        // (
        //     start_quat.mul_vec3(point1.position.extend(0.0)),
        //     start_quat.mul_vec3(point1.position.extend(0.0)) + vector_start * length,
        //     end_quat.mul_vec3(point2.position.extend(0.0)) + definition.end_offset - length * end_vector,
        //     end_quat.mul_vec3(point2.position.extend(0.0)) + definition.end_offset
        // );

        let mut previous_point: Vec3 = points.0;
        // println!("Length: {:} Vector Start {:?} Vector End {:?} Anchor points: {:?}", length, vector_start, end_vector, points);

        for x in 0..resolution {
            let point = bezier_3d(
                points.0,
                points.1,
                points.2,
                points.3,
                (x as f32) / (resolution as f32 - 1.0),
            );
            let tangent = bezier_tangent_3d(
                points.0,
                points.1,
                points.2,
                points.3,
                (x as f32) / (resolution as f32 - 1.0),
            );

            raw_vertexes.push(
                point
            );
            raw_tangents.push(
                tangent
            );
            raw_uvs.push(
                (bezier(
                    point1.uvs.0, point1.uvs.0 + (point2.uvs.0 - point1.uvs.0) / 3.0, point2.uvs.0 - (point2.uvs.0 - point1.uvs.0) / 3.0, point2.uvs.0,
                    (x as f32) / (resolution as f32 - 1.0),
                ),
                 bezier(
                     point1.uvs.1, point1.uvs.1 + (point2.uvs.1 - point1.uvs.1) / 3.0, point2.uvs.1 - (point2.uvs.1 - point1.uvs.1) / 3.0, point2.uvs.1,
                     (x as f32) / (resolution as f32 - 1.0),
                 )
                )
            );

            total_length += previous_point.distance(point) / (profile_length as f32);
            previous_point = point;
        }
    };
    for (index, ((vertex, tangent), uv)) in
    raw_vertexes.iter().zip(raw_tangents).zip(raw_uvs).enumerate() {
        let (u, v) = (index / resolution, index % resolution);

        let previous_vertex = raw_vertexes[((u + profile_length - 1) % profile_length * resolution + v) % raw_vertexes.len()];
        let next_vertex = raw_vertexes[((u + 1) % profile_length * resolution + v) % raw_vertexes.len()];
        let previous_normal = ((*vertex - previous_vertex)).cross(tangent).normalize();
        let next_normal = ((next_vertex - *vertex)).cross(tangent).normalize();


        vertexes.push(
            *vertex
        );
        vertexes.push(
            *vertex
        );
        normals.push(
            previous_normal
        );
        normals.push(next_normal);
        tangents.push(-tangent.cross(previous_normal).normalize());
        tangents.push(-tangent.cross(next_normal).normalize());
        uvs.push(
            [
                uv.0,
                total_length * TEXTURE_VERTICAL_SCALE * (v as f32) / (resolution as f32 - 1.0)
            ].into()
        );
        uvs.push(
            [
                uv.1,
                total_length * TEXTURE_VERTICAL_SCALE * (v as f32) / (resolution as f32 - 1.0)
            ].into()
        );

        // assert!(vertexes.len() == u * 2 *)
        // if v == 0 {
        //     println!("normal = {:} tangent = {:} offset = {:}", previous_normal, tangent, (next_vertex - *vertex).normalize());
        // }
    }
    //println!("Length: {:}", vertexes.len());

    for u in 0..profile_length {
        for v in 0..resolution - 1 {
            indicies.push(
                (u * resolution + v) as u32 * 2 + 1
            );
            indicies.push(
                (((u + 1) % (profile_length)) * resolution + v) as u32 * 2
            );
            indicies.push(
                (u * resolution + v + 1) as u32 * 2 + 1
            );
            indicies.push(
                (((u + 1) % (profile_length)) * resolution + v) as u32 * 2
            );
            indicies.push(
                (((u + 1) % (profile_length)) * resolution + v + 1) as u32 * 2
            );
            indicies.push(
                (u * resolution + v + 1) as u32 * 2 + 1
            );
        }
    };

    if definition.start_cap {
        let start_index = vertexes.len() as u32;
        for vertex in definition.start_profile.points {
            vertexes.push(
                start_quat.mul_vec3(vertex.position.extend(0.0)),
            );
            normals.push(
                start_quat.mul_vec3(Vec3::new(0., 0., -1.))
            );
            uvs.push(
                vertex.position / 12. + Vec2::new(0.128, 0.128)
            );
            tangents.push(
                Vec3::new(0.0, 1.0, 0.0)
            );
        };
        assert_eq!(vertexes.len(), normals.len());
        assert_eq!(vertexes.len(), uvs.len());
        assert_eq!(vertexes.len(), tangents.len());

        for point in definition.start_profile.cap_shape {
            indicies.push(point.2 + start_index);
            indicies.push(point.1 + start_index);
            indicies.push(point.0 + start_index);
        }
    }


    if definition.end_cap {
        let start_index = vertexes.len() as u32;
        for vertex in definition.end_profile.points.iter() {
            vertexes.push(
                definition.end_offset + end_quat.mul_vec3(vertex.position.extend(0.0)),
            );
            normals.push(
                end_quat.mul_vec3(Vec3::new(0., 0., 1.))
            );
            uvs.push(
                vertex.position / 12. + Vec2::new(0.128, 0.128)
            );
            tangents.push(
                Vec3::new(0.0, 1.0, 0.0)
            );
        };
        for point in definition.end_profile.cap_shape.iter().rev() {
            indicies.push(point.0 + start_index);
            indicies.push(point.1 + start_index);
            indicies.push(point.2 + start_index);
        }
    }

    save_mesh(
        hash,
        &vertexes,
        &normals,
        &uvs,
        &indicies,
    );

    let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
    mesh.set_attribute(
        Mesh::ATTRIBUTE_POSITION, vertexes.iter().map(|x| [x.x, x.y, x.z]).collect::<Vec<[f32; 3]>>(),
    );
    mesh.set_attribute(
        Mesh::ATTRIBUTE_NORMAL, normals.iter().map(|x| [x.x, x.y, x.z]).collect::<Vec<[f32; 3]>>(),
    );
    mesh.set_attribute(
        Mesh::ATTRIBUTE_UV_0, uvs.iter().map(|x| [x.x, x.y]).collect::<Vec<[f32; 2]>>(),
    );
    mesh.set_attribute(
        Mesh::ATTRIBUTE_TANGENT, tangents.iter().map(|x| [x.x, x.y, x.z]).collect::<Vec<[f32; 3]>>(),
    );
    mesh.set_indices(
        Some(Indices::U32(indicies))
    );
    return mesh;
}

fn float_max<T>(it: T) -> f32 where T: Iterator<Item=f32> {
    it.fold(-1. / 0., f32::max)
}

fn float_min<T>(it: T) -> f32 where T: Iterator<Item=f32> {
    it.fold(
        1. / 0., f32::min,
    )
}

#[allow(unused)]
fn save_mesh(
    hash: u64,
    vertexes: &Vec<Vec3>,
    normals: &Vec<Vec3>,
    uvs: &Vec<Vec2>,
    indices: &Vec<u32>,
) {
    let min_vertex = [
        float_min(vertexes.iter().map(|i| i.x)),
        float_min(vertexes.iter().map(|i| i.y)),
        float_min(vertexes.iter().map(|i| i.z)),
    ];
    let max_vertex = [
        float_max(vertexes.iter().map(|i| i.x)),
        float_max(vertexes.iter().map(|i| i.y)),
        float_max(vertexes.iter().map(|i| i.z)),
    ];


    let gltf_json_part = object! {
        "asset"=> object!{
            "generator": "None",
            "version": "2.0"
        },
        "scene"=> 0,
        "scenes"=>array![
            object!{
                "name"=> "Scene0",
                "nodes" => array![0]
            }
        ],
        "nodes"=>array![
            object!{
                "mesh"=>0,
                "name"=>"curve"
            }
        ],
        "meshes"=> array![
            object!{
                "primitives"=> array![
                    object!{
                        "attributes"=>object!{
                            "NORMAL"=> 0,
                            "POSITION"=>1,
						    "TEXCOORD_0"=>2
                        },
                        "indices"=>3
                    }
                ]
            }
        ],
        "accessors"=>array![
            object!{
                "bufferView"=>0,
                "componentType"=> 5126_u32, // Float
                "count"=> normals.len(),
                "type"=> "VEC3"
            },
            object!{
                "bufferView"=>1,
                "componentType"=> 5126_u32, // Float
                "count"=> vertexes.len(),
                "type"=> "VEC3",
                "min"=>array![min_vertex[0], min_vertex[1], min_vertex[2]],
                "max"=>array![max_vertex[0], max_vertex[1], max_vertex[2]],
            },
            object!{
                "bufferView"=>2,
                "componentType"=> 5126_u32, // Float
                "count"=> uvs.len(),
                "type"=> "VEC2"
            },
            object!{
                "bufferView"=>3,
                "componentType"=> 5125_u32, // Unsigned Int
                "count"=> indices.len(),
                "type"=> "SCALAR"
            }
        ],
        "bufferViews"=> array![
            object!{
                "buffer"=>0,
                "byteLength"=>4 *3 * normals.len(),
                "byteOffset"=>0,
            },
            object!{
                "buffer"=>0,
                "byteOffset"=>4 * 3 * normals.len(),
                "byteLength"=>4 * 3 * vertexes.len(),
            },

            object!{
                "buffer"=>0,
                "byteOffset"=>4 * 3 * normals.len() + 4 * 3 * vertexes.len(),
                "byteLength"=>4 * 2 * uvs.len(),
            },
            object!{
                "buffer"=>0,
                "byteOffset"=>4 * 3 * normals.len() + 4 * 3 * vertexes.len() + 4 * 2 * uvs.len(),
                "byteLength"=>4 * indices.len(),
            }
        ],
        "buffers"=>array![
            object!{
                "byteLength"=>4 * 3 * normals.len() + 4 * 3 * vertexes.len() + 4 * 2 * uvs.len() + 4 * indices.len()
            }
        ]
    };


    let mut jsfile = File::create(format!("cache/{:}.json", hash)).unwrap();
    jsfile.write_all(
        json::stringify_pretty(gltf_json_part.clone(), 2).as_bytes()
    );

    let mut data = json::stringify(gltf_json_part);
    while data.len() % 4 != 0 {
        data += " "
    };


    let buffer_normals: Vec<u8> = normals
        .iter()
        .map(|x| [x.x.to_le_bytes(), x.y.to_le_bytes(), x.z.to_le_bytes()])
        .flatten()
        .flatten()
        .collect();
    let buffer_positions: Vec<u8> = vertexes.iter().map(
        |x| [x.x.to_le_bytes(), x.y.to_le_bytes(), x.z.to_le_bytes()]).flatten().flatten().collect();
    let buffer_uvs: Vec<u8> = uvs.iter().map(|x| [x.x.to_le_bytes(), x.y.to_le_bytes()]).flatten().flatten().collect();
    let buffer_indices: Vec<u8> = indices.iter().map(|x| x.to_le_bytes()).flatten().collect();

    let mut file = File::create(format!("cache/{:}.glb", hash)).unwrap();
    file.write_all("glTF".as_bytes());
    file.write_all(&2_u32.to_le_bytes());
    file.write_all(
        &(
            (data.len() +
                buffer_normals.len() +
                buffer_positions.len() +
                buffer_uvs.len() +
                buffer_indices.len() +
                16 + // Chunk headers
                12 // Top header
            ) as u32
        ).to_le_bytes()
    );

    file.write_all(&(data.len() as u32).to_le_bytes()).unwrap();
    file.write_all("JSON".as_bytes()).unwrap();
    file.write(data.as_bytes()).unwrap();

    file.write_all(&((buffer_positions.len() + buffer_normals.len() + buffer_uvs.len() + buffer_indices.len()) as u32).to_le_bytes());
    file.write_all("BIN".as_bytes()).unwrap();
    file.write(&[0]).unwrap();
    file.write_all(buffer_normals.as_slice()).unwrap();
    file.write_all(buffer_positions.as_slice()).unwrap();
    file.write_all(buffer_uvs.as_slice()).unwrap();
    file.write_all(buffer_indices.as_slice()).unwrap();
}