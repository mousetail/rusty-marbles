use bevy::prelude::*;
use crate::AppState::InGame;

pub struct SlowRotate {
    pub speed: f32,
}

pub struct SlowBob {
    pub frequency: f32,
    pub amplitude: f32,

    pub t: f32,
}

impl Default for SlowBob {
    fn default() -> Self {
        SlowBob {
            frequency: 0.5,
            amplitude: 4.0,
            t: 0.0,
        }
    }
}

pub struct ZeppelinPlugin {}

impl Plugin for ZeppelinPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system_set(
            SystemSet::on_enter(InGame)
                .with_system(create_zeppelins.system()
                )
        )
            .add_system_set(
                SystemSet::on_update(InGame)
                    .with_system(slow_rotate_process.system())
                    .with_system(slow_bob_process.system()
                    )
            );
    }
}

fn add_zeppelin(commands: &mut Commands, model: Handle<Scene>, position: Vec3, center: Vec3) {
    commands.spawn_bundle(
        (
            Transform {
                translation: center,
                ..Default::default()
            },
            GlobalTransform::identity()
        )
    ).insert(
        SlowRotate {
            speed: -0.025,
        }
    )
        .with_children(
            |parent| {
                parent.spawn_bundle(
                    (
                        Transform {
                            translation: position - center,
                            rotation: Quat::from_rotation_y((position.x - center.x).atan2(position.z - center.z)),
                            ..Default::default()
                        },
                        GlobalTransform::identity()
                    )
                ).insert(
                    SlowBob {
                        ..Default::default()
                    }
                ).with_children(
                    |parent2| {
                        parent2.spawn_scene(model);
                    }
                );
            }
        );
}

pub fn create_zeppelins(mut commands: Commands, server: Res<AssetServer>) {
    let scene = server.load("models/sign.glb#Scene0");

    add_zeppelin(
        &mut commands,
        scene.clone(),
        Vec3::new(-25. - 45., 0., 75.),
        Vec3::new(-25., 0., 75.),
    );

    add_zeppelin(
        &mut commands,
        scene.clone(),
        Vec3::new(-25. + 45., 0., 75.),
        Vec3::new(-25., 0., 75.),
    );


    add_zeppelin(
        &mut commands,
        scene,
        Vec3::new(-25., 0., 75. + 45.),
        Vec3::new(-25., 0., 0.),
    )
}

pub fn slow_rotate_process(mut query: Query<(&mut Transform, &SlowRotate)>, time: Res<Time>) {
    for (mut transform, rotate) in query.iter_mut() {
        transform.rotation *= Quat::from_rotation_y(rotate.speed * time.delta_seconds());
    }
}

pub fn slow_bob_process(mut query: Query<(&mut Transform, &mut SlowBob)>, time: Res<Time>) {
    for (mut transform, mut bob) in query.iter_mut() {
        transform.translation.y += bob.amplitude * (((bob.t + time.delta_seconds()) * bob.frequency).sin() - (bob.t * bob.frequency).sin());
        bob.t = bob.t + time.delta_seconds();
    }
}