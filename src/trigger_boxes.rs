use bevy::prelude::*;
use bevy_rapier3d::prelude::*;
use crate::BallLabel;

pub struct FinishBox;

pub struct WinnersList {
    pub ranking: Vec<String>,
}

pub fn handle_finish_box(mut balls: Query<(Entity, &mut BallLabel)>, triggers: Query<(Entity, &FinishBox)>,
                     intersection_matrix: Res<NarrowPhase>,
                     mut winners: ResMut<WinnersList>,
) {
    for mut ball in balls.iter_mut() {
        if !ball.1.finished {
            for trigger in triggers.iter() {
                if match intersection_matrix.intersection_pair(
                    ball.0.handle(), trigger.0.handle()) {
                    Some(true) => true,
                    _ => false
                } {
                    println!("Player {:} wins!", ball.1.label);
                    winners.ranking.push(ball.1.label.clone());
                    ball.1.finished = true;
                }
            }
        }
    }
}