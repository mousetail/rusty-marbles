use std::collections::HashMap;
use std::fs;
use bevy::prelude::*;
use bevy_rapier3d::prelude::*;
use crate::cube_bundle::CubeoidRigidbodyBundle;
use crate::generate_geometry::{bezier_3d, bezier_tangent_3d, create_track_mesh, get_bezier_points};
use crate::MeshCollider;
use crate::profiles::*;
use std::hash::Hasher;
use crate::trigger_boxes::FinishBox;
use serde::{Serialize, Deserialize};
use crate::slow_rotate::SlowBob;


#[derive(Serialize, Deserialize, Eq, PartialEq)]
enum ObstacleType {
    Bot,
    Donny,
    Wookie,
    Karm,
    Jas,
    BarricadeA,
    Canopy,
}

#[derive(Serialize, Deserialize)]
struct Obstacle {
    kind: ObstacleType,
    x: f32,
    y: f32,
    #[serde(default)]
    angle: f32,
}

#[derive(Serialize, Deserialize)]
enum LevelPiece {
    StartBlock,
    BendLeft,
    BendRight,
    DropdownStairs { steps: usize },
    WideToNormal,
    NormalToWide,
    DynamicShape {
        definition: TrackDefinition,
        #[serde(default)]
        obstacles: Vec<Obstacle>,
    },
    Pillar,
    Drop { depth: u32 },
    Finish,
    Light,
}

fn look_rotation(forward: Vec3, up: Vec3) -> Quat {
    // ^ assumes forward and up are already unit length
    let lateral = up.cross(forward).normalize();
    let up = forward.cross(lateral).normalize();
    let rotation_matrix = Mat3::from_cols(lateral, up, forward);
    Quat::from_rotation_mat3(&rotation_matrix)
}

fn add_model_and_collider(
    collider_path: String,
    render_path: String,
    position: Vec3,
    angle: f32,
    commands: &mut Commands,
    material: Handle<StandardMaterial>,
    asset_server: &Res<AssetServer>,
) {
    let collider_mesh: Handle<Mesh> = asset_server.load(
        collider_path.as_str()
    );
    let render_mesh: Handle<Mesh> = asset_server.load(
        render_path.as_str()
    );
    commands.spawn_bundle(
        RigidBodyBundle {
            body_type: RigidBodyType::Static,
            position: (position, Quat::from_rotation_y(angle + std::f32::consts::FRAC_PI_2)).into(),
            ..Default::default()
        }
    ).insert_bundle(
        ColliderBundle {
            ..Default::default()
        }
    ).insert_bundle(
        PbrBundle {
            mesh: render_mesh.clone(),
            material: material,
            ..Default::default()
        }
    ).insert(
        ColliderPositionSync::Discrete)
        .insert(
            MeshCollider {
                mesh: collider_mesh.clone()
            }
        );
}

fn add_model_with_collisions(
    path: String,
    position: Vec3,
    angle: f32,
    commands: &mut Commands,
    material: Handle<StandardMaterial>,
    asset_server: &Res<AssetServer>,
) {
    add_model_and_collider(
        path.clone(),
        path,
        position,
        angle,
        commands,
        material,
        asset_server,
    )
}

fn calculate_hash<T: std::hash::Hash>(t: &T) -> u64 {
    let mut s = std::collections::hash_map::DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}

pub fn generate_level(mut commands: Commands,
                      mut meshes: ResMut<Assets<Mesh>>,
                      mut materials: ResMut<Assets<StandardMaterial>>,
                      asset_server: Res<AssetServer>,
) {
    let level_pieces: Vec<LevelPiece> = serde_json::from_str(
        &fs::read_to_string("assets/levels/level1_serde.json").unwrap()
        //read_data()
    ).unwrap();


    //let json_text = serde_json::to_string_pretty(&level_pieces).unwrap();
    //let mut output_file = File::create("assets/levels/level1_serde.json").unwrap();
    //output_file.write(json_text.as_bytes()).unwrap();


    let track_depth = 0.05;
    let track_drop = 1.9;
    let track_lead = 3.5;
    let track_angle = std::f32::consts::FRAC_PI_8 * 0.75;
    let track_width = 2.5;
    let track_length = 3.5;

    let mut position = Vec3::new(0.0, 0.0, 0.0);
    let mut angle: f32 = 0.0;

    let albedo_texture: Handle<Texture> = asset_server.load("textures/grate/rust.png");
    // let normal_texture: Handle<Texture> = asset_server.load("textures/grate/normal.png");
    let material: Handle<StandardMaterial> = materials.add(StandardMaterial {
        base_color_texture: Some(albedo_texture),
        // normal_map: Some(normal_texture),
        ..Default::default()
    });

    let track_albedo: Handle<Texture> = asset_server.load("textures/raw/albedo.png");
    let track_normal: Handle<Texture> = asset_server.load("textures/raw/normal.png");
    let track_material: Handle<StandardMaterial> = materials.add(
        StandardMaterial {
            base_color_texture: Some(track_albedo),
            normal_map: Some(track_normal),
            ..Default::default()
        }
    );

    let pillar: Handle<Mesh> = asset_server.load("models/pillar3.glb#Mesh0/Primitive0");
    let pillar_material: Handle<StandardMaterial> = materials.add(
        StandardMaterial {
            base_color_texture: Some(
                asset_server.load("textures/pillar3_ao.png")
            ),
            unlit: true,
            ..Default::default()
        }
    );

    let mut piece_map: HashMap<TrackDefinition, (Handle<Mesh>, Handle<Mesh>, Vec3, f32)> = Default::default();

    let bot_model = asset_server.load("models/bot.glb#Mesh0/Primitive0");
    let bot_material = materials.add(
        StandardMaterial {
            base_color_texture: Some(asset_server.load("textures/bot.png")),
            ..Default::default()
        }
    );

    let donny_model = asset_server.load("models/donny.glb#Mesh0/Primitive0");
    let donny_material = materials.add(
        StandardMaterial {
            base_color_texture: Some(asset_server.load("textures/donny.png")),
            emissive: Color::rgb(0.1, 0.1, 0.1),
            ..Default::default()
        }
    );
    let wookie_material = materials.add(
        StandardMaterial {
            base_color_texture: Some(asset_server.load("textures/wookie.png")),
            emissive: Color::rgb(0.1, 0.1, 0.1),
            ..Default::default()
        }
    );

    let karm_material = materials.add(
        StandardMaterial {
            base_color_texture: Some(asset_server.load("textures/karm.png")),
            emissive: Color::rgb(0.1, 0.1, 0.1),
            ..Default::default()
        }
    );


    let jas_material = materials.add(
        StandardMaterial {
            base_color_texture: Some(asset_server.load("textures/jas.png")),
            emissive: Color::rgb(0.1, 0.1, 0.1),
            ..Default::default()
        }
    );

    let road_material: Handle<StandardMaterial> = materials.add(
        StandardMaterial {
            base_color_texture: Some(
                asset_server.load("textures/road/albedo.png")
            ),
            normal_map: Some(
                asset_server.load("textures/road/normal.png")
            ),
            roughness: 0.6,
            ..Default::default()
        }
    );

    let barricade_a_model: Handle<Mesh> = asset_server.load("models/barricade_a.glb#Mesh0/Primitive0");

    let barricade_a_material: Handle<StandardMaterial> = materials.add(
        StandardMaterial {
            base_color_texture: Some(
                asset_server.load("textures/barricade/barricade_A_D.png")
            ),
            normal_map: Some(
                asset_server.load("textures/barricade/barricade_A_N.png")
            ),
            ..Default::default()
        }
    );

    let canopy_model: Handle<Mesh> = asset_server.load("models/canopy.glb#Mesh0/Primitive0");

    let light_model: Handle<Mesh> = asset_server.load("models/light_fixture.glb#Mesh0/Primitive0");
    let light_texture = Some(asset_server.load("textures/light_emission.png"));
    let light_material: Handle<StandardMaterial> = materials.add(
        StandardMaterial {
            base_color_texture: light_texture.clone(),
            emissive: Color::WHITE,
            emissive_texture: light_texture,
            unlit: true,
            ..Default::default()
        }
    );

    for piece in level_pieces {
        let angle_quaternion = Quat::from_rotation_y(angle);

        match piece {
            LevelPiece::BendRight => {
                add_model_and_collider(
                    "models/bend.glb#Mesh0/Primitive0".to_string(),
                    "models/bend_right_smooth.glb#Mesh0/Primitive0".to_string(),
                    position,
                    angle,
                    &mut commands,
                    road_material.clone(),
                    &asset_server,
                );

                position = position +
                    angle_quaternion.mul_vec3(Vec3::new(-10.0, -2.0, -10.0))
                ;
                angle -= std::f32::consts::FRAC_PI_2;
            }
            LevelPiece::BendLeft => {
                add_model_and_collider(
                    "models/bend_left.glb#Mesh0/Primitive0".to_string(),
                    "models/bend_left_smooth.glb#Mesh0/Primitive0".to_string(),
                    position,
                    angle,
                    &mut commands,
                    road_material.clone(),
                    &asset_server,
                );

                position = position +
                    angle_quaternion.mul_vec3(Vec3::new(-10.0, -2.0, 10.0))
                ;
                angle += std::f32::consts::FRAC_PI_2;
            }
            LevelPiece::WideToNormal => {
                add_model_with_collisions(
                    "models/wide_to_normal.glb#Mesh0/Primitive0".to_string(),
                    position + angle_quaternion.mul_vec3(Vec3::new(0.0, 0.0, 4.0)),
                    angle,
                    &mut commands,
                    material.clone(),
                    &asset_server,
                );

                position = position +
                    angle_quaternion.mul_vec3(Vec3::new(-4.0, 0.0, 0.0))
                ;
            }
            LevelPiece::NormalToWide => {
                add_model_with_collisions(
                    "models/wide_to_normal.glb#Mesh0/Primitive0".to_string(),
                    position,
                    angle + std::f32::consts::PI,
                    &mut commands,
                    material.clone(),
                    &asset_server,
                );

                position = position +
                    angle_quaternion.mul_vec3(Vec3::new(-4.0, 0.0, 0.0))
                ;
            }
            LevelPiece::DropdownStairs { steps: n } => {
                for index in 0..n {
                    let offset_vector = Vec3::new(0.0, -track_drop - track_drop * index as f32, 0.0) + angle_quaternion.mul_vec3(
                        Vec3::new(
                            0.0,
                            0.0,
                            if index % 2 == 0 { track_lead * 0.5 + 3.0 } else { -track_lead * 0.5 + 3.0 },
                        ));

                    let block_body = RigidBodyBundle {
                        body_type: RigidBodyType::Static,
                        position: (
                            position +

                                offset_vector,
                            Quat::from_rotation_y(
                                angle
                            ) *
                                Quat::from_rotation_x(
                                    if index % 2 == 0 { -track_angle } else { track_angle })).into(),

                        ..Default::default()
                    };
                    let block_collider = ColliderBundle {
                        shape:
                        ColliderShape::cuboid(track_length, track_depth, track_width),
                        ..Default::default()
                    };
                    commands.spawn_bundle(
                        block_collider
                    ).insert_bundle(
                        block_body
                    ).insert_bundle(
                        PbrBundle {
                            mesh: meshes.add(Mesh::from(bevy::prelude::shape::Box::new(
                                track_length * 2.0,
                                track_depth * 2.0,
                                track_width * 2.0,
                            ))),
                            material: material.clone(),
                            ..Default::default()
                        }
                    )
                        .insert(
                            ColliderPositionSync::Discrete);

                    for railing_x in [-track_width / 1.0, track_width / 1.0].iter() {
                        commands.spawn_bundle(
                            RigidBodyBundle {
                                body_type: RigidBodyType::Static,

                                position: (
                                    position +
                                        angle_quaternion.mul_vec3(
                                            Vec3::new(*railing_x, 0.0, 0.0),
                                        ) +
                                        offset_vector,
                                    Quat::from_rotation_y(
                                        angle
                                    ) *
                                        Quat::from_rotation_x(
                                            if index % 2 == 0 { -track_angle } else { track_angle }) *
                                        Quat::from_rotation_z(
                                            std::f32::consts::FRAC_PI_4
                                        )
                                ).into(),
                                ..Default::default()
                            }
                        ).insert_bundle(
                            ColliderBundle {
                                shape: ColliderShape::cuboid(4.0 * track_depth, 4.0 * track_depth, track_length),
                                ..Default::default()
                            }
                        ).insert_bundle(
                            PbrBundle {
                                mesh: meshes.add(Mesh::from(bevy::prelude::shape::Box::new(
                                    track_depth * 8.0,
                                    track_depth * 8.0,
                                    track_length * 2.0 + 0.2,
                                ))),
                                material: materials.add(Color::rgb(0.85, 0.25, 0.25).into()),
                                ..Default::default()
                            }
                        )
                            .insert(
                                ColliderPositionSync::Discrete);
                    }
                }
                position += angle_quaternion.mul_vec3(Vec3::new(0.0, -track_drop * (n as f32 + 1.0), 3.0))
            }
            LevelPiece::Pillar => {
                for i in 0..5 {
                    commands.spawn_bundle(
                        PbrBundle {
                            mesh: pillar.clone(),
                            material: pillar_material.clone(),
                            transform: Transform {
                                translation: position + Vec3::new(0., -1.6 - (i as f32) * 24.6444, 0.),
                                rotation: angle_quaternion,
                                ..Default::default()
                            },
                            ..Default::default()
                        }
                    );
                }
            }
            LevelPiece::Drop { depth: amount } => {
                position -= Vec3::new(0., amount as f32, 0.)
            }
            LevelPiece::StartBlock => {
                let railing_angle = std::f32::consts::FRAC_PI_6;
                commands.spawn_bundle(
                    CubeoidRigidbodyBundle::new(
                        (
                            track_width * 2.0,
                            track_depth * 2.0,
                            track_width * 4.0),
                        (
                            position,
                            Quat::from_rotation_y(
                                angle + std::f32::consts::FRAC_PI_2
                            ) *
                                Quat::from_rotation_z(
                                    track_angle
                                )
                        ).into(),
                        material.clone(), //materials.add(Color::rgb(0.85, 0.75, 0.25).into()),
                        &mut meshes,
                    )
                );

                let railing_length = 2.0 * track_width / railing_angle.cos();
                commands.spawn_bundle(
                    CubeoidRigidbodyBundle::new(
                        (railing_length, 8.0 * track_depth, 4.0 * track_depth),
                        (
                            position + angle_quaternion.mul_vec3(
                                [-track_width * 2., 0.0, 0.0].into()
                            ),
                            Quat::from_rotation_y(
                                angle + std::f32::consts::FRAC_PI_2
                            ) *
                                Quat::from_rotation_z(
                                    track_angle
                                ) * Quat::from_rotation_y(
                                railing_angle
                            )
                        ).into(),
                        materials.add(Color::rgb(0.85, 0.25, 0.25).into()),
                        &mut meshes,
                    )
                );

                commands.spawn_bundle(
                    CubeoidRigidbodyBundle::new(
                        (railing_length, 8.0 * track_depth, 4.0 * track_depth),
                        (
                            position + angle_quaternion.mul_vec3(
                                [track_width * 2., 0.0, 0.0, ].into()
                            ),
                            Quat::from_rotation_y(
                                angle + std::f32::consts::FRAC_PI_2
                            ) *
                                Quat::from_rotation_z(
                                    track_angle
                                ) * Quat::from_rotation_y(
                                -railing_angle
                            )
                        ).into(),
                        materials.add(Color::rgb(0.85, 0.25, 0.25).into()),
                        &mut meshes,
                    )
                );


                position += angle_quaternion.mul_vec3([0.0, -1.0, 0.8].into())
            }
            LevelPiece::DynamicShape { definition: shape, obstacles } => {
                if !piece_map.contains_key(&shape) {
                    let track_geometry = get_track_geometry(
                        shape
                    );

                    let high_res_mesh = meshes.add(
                        create_track_mesh(track_geometry.clone(), 14, calculate_hash(&shape)),
                    );
                    let low_res_mesh = meshes.add(
                        create_track_mesh(track_geometry.clone(), 6, calculate_hash(&shape) + 1)
                    );

                    piece_map.insert(shape, (low_res_mesh,
                                             high_res_mesh,
                                             track_geometry.end_offset,
                                             track_geometry.end_y_angle),
                    );
                }

                let (low_res_mesh, high_res_mesh, offset, offset_rotation) = piece_map[&shape].clone();
                commands.spawn_bundle(
                    RigidBodyBundle {
                        body_type: RigidBodyType::Static,
                        position: (position, Quat::from_rotation_y(angle)).into(),
                        ..Default::default()
                    }
                ).insert_bundle(
                    ColliderBundle {
                        ..Default::default()
                    }
                ).insert_bundle(
                    PbrBundle {
                        mesh: high_res_mesh.clone(),
                        material: track_material.clone(),
                        ..Default::default()
                    }
                ).insert(
                    ColliderPositionSync::Discrete
                ).insert(
                    MeshCollider {
                        mesh: low_res_mesh.clone()
                    }
                );

                for obstacle in obstacles {
                    let points = get_bezier_points(
                        position,
                        Quat::from_rotation_y(angle) * Quat::from_rotation_x(0.14888994760949725),
                        position + (Quat::from_rotation_y(angle)).mul_vec3(offset),
                        Quat::from_rotation_y(angle + offset_rotation) * Quat::from_rotation_x(0.14888994760949725),
                    );

                    let obstacle_position = bezier_3d(
                        points.0,
                        points.1,
                        points.2,
                        points.3,
                        obstacle.y,
                    );

                    let obstacle_tangent = bezier_tangent_3d(
                        points.0,
                        points.1,
                        points.2,
                        points.3,
                        obstacle.y,
                    ).normalize();

                    let horizontal_tangent = obstacle_tangent.cross(Vec3::new(0.0, 1.0, 0.0)).normalize();
                    println!("Difference: {:?}", points.1 - points.0);
                    println!("Position: {:?} y0: {:} y1: {:} tangent: {:?} t2: {:?}", obstacle_position, position.y, position.y + offset.y, horizontal_tangent, obstacle_tangent);

                    commands.spawn_bundle(
                        ColliderBundle {
                            shape: match obstacle.kind {
                                ObstacleType::BarricadeA => ColliderShape::cuboid(
                                    1.0,
                                    0.6,
                                    0.3,
                                ),
                                ObstacleType::Canopy => ColliderShape::compound(
                                    vec![
                                        (
                                            [0.0, 9.0, 0.0].into(),
                                            ColliderShape::cuboid(
                                                5.5,
                                                1.0,
                                                5.5,
                                            )
                                        ),
                                        (
                                            [0.0, 5.0, 0.0].into(),
                                            ColliderShape::cuboid(
                                                0.5, 5.0, 0.5,
                                            )
                                        ),
                                    ]
                                ),
                                _ => ColliderShape::cylinder(
                                    1.0,
                                    0.5,
                                )
                            },
                            position: ((obstacle_position + horizontal_tangent * obstacle.x),
                                       look_rotation(
                                           obstacle_tangent,
                                           Vec3::Y,
                                       ) * Quat::from_rotation_y(obstacle.angle)
                                       // Quat::from_rotation_x(std::f32::consts::FRAC_PI_2)
                                       // Quat::from_angle
                            ).into(),

                            ..Default::default()
                        }
                    ).insert_bundle(
                        PbrBundle {
                            mesh: match obstacle.kind {
                                ObstacleType::Bot => bot_model.clone(),
                                ObstacleType::BarricadeA => barricade_a_model.clone(),
                                ObstacleType::Canopy => canopy_model.clone(),
                                _ => donny_model.clone()
                            },
                            material: match obstacle.kind {
                                ObstacleType::Bot => bot_material.clone(),
                                ObstacleType::BarricadeA => barricade_a_material.clone(),
                                ObstacleType::Donny => donny_material.clone(),
                                ObstacleType::Wookie => wookie_material.clone(),
                                ObstacleType::Karm => karm_material.clone(),
                                ObstacleType::Jas |
                                ObstacleType::Canopy => jas_material.clone()
                            },
                            visible: Visible {
                                is_transparent: obstacle.kind == ObstacleType::Bot,
                                ..Default::default()
                            },
                            ..Default::default()
                        }
                    ).insert(
                        ColliderPositionSync::Discrete
                    );
                }

                position += Quat::from_rotation_y(angle).mul_vec3(offset);
                angle += offset_rotation;
            }
            LevelPiece::Finish => {
                commands.spawn_bundle(
                    ColliderBundle {
                        shape: ColliderShape::cuboid(5.0, 5.0, 5.0),
                        position: position.into(),
                        collider_type: ColliderType::Sensor,
                        ..Default::default()
                    }
                )
                    .insert_bundle(
                        PbrBundle {
                            mesh: meshes.add(
                                bevy::prelude::shape::Box::new(10.0, 10.0, 10.0).into()
                            ),
                            // material: materials.add(
                            //     Color::rgba(1.0, 0.2, 0.2, 0.25).into()
                            // ),
                            material: materials.add(StandardMaterial { base_color_texture: Some(asset_server.load("textures/ring.png")), ..Default::default() }),
                            visible: Visible {
                                is_transparent: true,
                                ..Default::default()
                            },
                            ..Default::default()
                        }
                    )
                    .insert(
                        FinishBox {}
                    )
                    .insert(
                        ColliderPositionSync::Discrete
                    );
            }
            LevelPiece::Light => {
                commands.spawn_bundle(LightBundle {
                    transform: Transform::from_translation(position + Vec3::Y * 8.0),
                    light: Light {
                        intensity: 1000.0,
                        range: 40.0,
                        ..Default::default()
                    },
                    ..Default::default()
                }).insert(
                    SlowBob {
                        amplitude: 0.5,
                        frequency: 0.6,
                        t: -((position.x - position.y).abs()),
                    }
                ).with_children(
                    |ent| {
                        ent.spawn_bundle(
                            PbrBundle {
                                mesh: light_model.clone(),
                                material: light_material.clone(),
                                ..Default::default()
                            }
                        );
                    }
                );
            }
        }
    }
}