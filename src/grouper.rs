pub struct Grouper<M, const N: usize> where
    M: Iterator,
    <M as Iterator>::Item: Copy {
    iterator: M,
}

impl<M, const N: usize> Grouper<M, N> where
    M: Iterator,
    <M as Iterator>::Item: Copy {
    pub fn new(it: M) -> Grouper<M, N> {
        Grouper {
            iterator: it
        }
    }
}

impl<M, const N: usize> Iterator for Grouper<M, N> where M: Iterator, <M as Iterator>::Item: Copy {
    type Item = [M::Item; N];

    fn next(&mut self) -> Option<Self::Item> {
        let mut buffer: [Option<M::Item>; N] = [None; N];
        for i in 0..N {
            buffer[i] = Some(self.iterator.next()?);
        }
        let result: [M::Item; N] = buffer.map(|x| x.unwrap());
        return Some(result);
    }
}