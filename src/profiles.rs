use bevy::prelude::{Vec2, Vec3};
use crate::generate_geometry::{ProfilePoint, TrackGeometry, TrackProfileGeometry};
use serde::{Serialize, Deserialize};

const SLOPE: f32 = 0.15;

fn is_default<T>(value: &T) -> bool where T: std::default::Default, T: Eq {
    let default_value: T = std::default::Default::default();
    return value == &default_value;
}

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub enum TrackProfileWidth {
    Wide,
    Normal,
    Narrow,
    Custom(usize),
}

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub enum TrackProfileEdgeShape {
    None,
    Sloped,
    Straight,
    SlopedHigh,
    StraightHigh,
}

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub struct TrackProfileType {
    pub width: TrackProfileWidth,
    pub edge_shape: TrackProfileEdgeShape,
}


#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub enum TrackShape {
    Long,
    Short,
    Steep,
    BendLeft,
    BendLeftLarge,
    SteepBendLeft,
    BendRight,
    BendRightLarge,
    SteepBendRight,
    SBendLeft,
    SBendRight,
}

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub struct TrackDefinition {
    pub start_profile: TrackProfileType,
    pub end_profile: TrackProfileType,
    pub shape: TrackShape,
    #[serde(default, skip_serializing_if = "is_default")]
    pub start_cap: bool,
    #[serde(default, skip_serializing_if = "is_default")]
    pub end_cap: bool,
}


fn get_profile(
    width: TrackProfileWidth,
    edge: TrackProfileEdgeShape,
) -> TrackProfileGeometry {
    let width: f32 = match width {
        TrackProfileWidth::Narrow => 1.25,
        TrackProfileWidth::Normal => 2.5,
        TrackProfileWidth::Wide => 4.0,
        TrackProfileWidth::Custom(i) => i as f32
    };

    let profile = match edge {
        TrackProfileEdgeShape::None => vec![
            Vec2::new(1.2, 0.),
            Vec2::new(0.6, 0.),
            Vec2::new(0.0, 0.),
            Vec2::new(0., -0.5),
        ],
        TrackProfileEdgeShape::Sloped => vec![
            Vec2::new(0., 0.),
            Vec2::new(-1.0, 1.0),
            Vec2::new(-1.5, 1.0),
            Vec2::new(-1.5, -0.5),
        ],
        TrackProfileEdgeShape::SlopedHigh => vec![
            Vec2::new(0., 0.),
            Vec2::new(-2.0, 2.5),
            Vec2::new(-2.5, 2.5),
            Vec2::new(-2.5, -0.5),
        ],
        TrackProfileEdgeShape::Straight => vec![
            Vec2::new(0., 0.),
            Vec2::new(-0.0, 0.5),
            Vec2::new(-0.5, 0.5),
            Vec2::new(-0.5, -0.5),
        ],
        TrackProfileEdgeShape::StraightHigh => vec![
            Vec2::new(0., 0.),
            Vec2::new(-0.0, 1.5),
            Vec2::new(-0.5, 1.5),
            Vec2::new(-0.5, -0.5),
        ]
    };

    let uvs = [
        ((0.5 - width / 10.).max(0.25 + 1. / 18.), 0.25 + 1. / 18.),
        (0.25 - 1. / 18., 0.25 - 1. / 18.),
        (0.25 - 1. / 18. - 0.05, 0.25 - 1. / 18. - 0.05),
        (0.25 - 1. / 18. - 0.1, 0.25 - 1. / 18. - 0.1)
    ];

    let profile: Vec<ProfilePoint> = profile.iter().enumerate().map(|(index, x)| {
        ProfilePoint { position: *x - Vec2::new(width, 0.), uvs: uvs[index] }
    })
        .chain(
            std::iter::once(ProfilePoint { position: Vec2::new(0.0, -0.5), uvs: (0.0, 1.0) })
        )
        .chain(
            profile.iter().enumerate().rev().map(
                |(index, x)| ProfilePoint { position: Vec2::new(width - x.x, x.y), uvs: (1. - uvs[index].1, 1. - uvs[index].0) }
            )
        ).chain(
        std::iter::once(
            ProfilePoint {
                position: Vec2::new(0.0,
                                    0.0),
                uvs: (0.5, 0.5),
            }
        )
    ).collect();

    return TrackProfileGeometry {
        points: profile,

        cap_shape: vec![
            // first half
            (0, 1, 2),
            (0, 2, 3),
            (0, 3, 4),
            (0, 4, 9),
            // second half
            (6, 7, 8),
            (5, 6, 8),
            (4, 5, 8),
            (9, 4, 8),
        ],
    };
}

pub fn get_track_geometry(definition: TrackDefinition) -> TrackGeometry {
    let (end_offset, end_angle, _) = match definition.shape {
        TrackShape::Long => (Vec3::new(0.0, -30.0 * SLOPE, 30.0), 0.0, false),
        TrackShape::Short => (Vec3::new(0.0, -15.0 * SLOPE, 15.0), 0.0, false),
        TrackShape::Steep => (Vec3::new(0.0, -30. * SLOPE * 2., 30.0), 0.0, true),
        TrackShape::BendLeft => (Vec3::new(10.0,
                                           -std::f32::consts::FRAC_PI_2 * 10.0 * SLOPE, 10.0),
                                 std::f32::consts::FRAC_PI_2, true),
        TrackShape::BendLeftLarge => (Vec3::new(20.0,
                                                -std::f32::consts::FRAC_PI_2 * 20.0 * SLOPE, 20.0),
                                      std::f32::consts::FRAC_PI_2, true),
        TrackShape::BendRight => (Vec3::new(-10.0,
                                            -std::f32::consts::FRAC_PI_2 * 10.0 * SLOPE, 10.0),
                                  -std::f32::consts::FRAC_PI_2, true),

        TrackShape::BendRightLarge => (Vec3::new(-20.0,
                                                 -std::f32::consts::FRAC_PI_2 * 20.0 * SLOPE, 20.0),
                                       -std::f32::consts::FRAC_PI_2, true),
        TrackShape::SteepBendLeft => (Vec3::new(
            -10.0, -std::f32::consts::FRAC_PI_2 * 10.0 * SLOPE * 1.5, 10.0),
                                      std::f32::consts::FRAC_PI_2, true),
        TrackShape::SteepBendRight => (
            Vec3::new(10.0, -std::f32::consts::FRAC_PI_2 * 10.0 * SLOPE * 1.5, 10.0),
            -std::f32::consts::FRAC_PI_2, true),
        TrackShape::SBendLeft => (Vec3::new(2.5, -10. * SLOPE, 10.0), 0.0, true),
        TrackShape::SBendRight => (Vec3::new(-2.5, -10. * SLOPE, 10.0), 0.0, true)
    };

    let angle = SLOPE.atan();

    return TrackGeometry {
        start_profile: get_profile(
            definition.start_profile.width,
            definition.start_profile.edge_shape,
        ),
        end_profile: get_profile(
            definition.end_profile.width,
            definition.end_profile.edge_shape,
        ),
        end_offset,
        end_y_angle: end_angle,
        start_x_angle: angle,
        end_x_angle: angle,
        start_cap: definition.start_cap,
        end_cap: definition.end_cap,
    };
}