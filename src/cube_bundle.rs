use bevy::prelude::*;
use bevy_rapier3d::prelude::*;
use crate::RigidBodyPosition;

#[derive(Bundle)]
pub struct CubeoidRigidbodyBundle {
    #[bundle]
    rigid_body: RigidBodyBundle,
    #[bundle]
    collider: ColliderBundle,
    #[bundle]
    pbr: PbrBundle,
    position_sync: ColliderPositionSync,
}

impl CubeoidRigidbodyBundle {
    pub fn new(
        size: (f32, f32, f32),
        position: RigidBodyPosition,
        material: Handle<StandardMaterial>,
        meshes: &mut ResMut<Assets<Mesh>>,
    ) -> Self {
        Self {
            rigid_body: RigidBodyBundle {
                body_type: RigidBodyType::Static,
                position: position,
                ..Default::default()
            },
            collider: ColliderBundle {
                shape: ColliderShape::cuboid(
                    size.0, size.1, size.2,
                )
                ,
                ..Default::default()
            },
            pbr: PbrBundle {
                mesh: meshes.add(Mesh::from(bevy::prelude::shape::Box::new(
                    size.0 * 2.0,
                    size.1 * 2.0,
                    size.2 * 2.0,
                ))),
                material: material,
                ..Default::default()
            },
            position_sync: ColliderPositionSync::Discrete,
        }
    }
}