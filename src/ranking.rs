use bevy::prelude::*;
use crate::{AppState, WinnersList};

pub struct RankingPlugin {}

impl Plugin for RankingPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system_set(
            SystemSet::on_enter(AppState::InGame)
                .with_system(setup_ranking_system.system()
                )
        ).add_system_set(
            SystemSet::on_update(AppState::InGame)
                .with_system(
                    update_ranking_system.system()
                )
        );
    }
}

struct Ranking {
    length: usize,
}

struct LeftBlackBar;

fn setup_ranking_system(mut commands: Commands, asset_server: Res<AssetServer>,
                        mut materials: ResMut<Assets<ColorMaterial>>) {
    let font = asset_server.load("fonts/roboto.ttf");

    commands.spawn_bundle(
        NodeBundle {
            style: Style {
                size: Size::new(Val::Percent(20.0), Val::Percent(100.0)),
                position_type: PositionType::Absolute,
                position: Rect {
                    left: Val::Percent(
                        -20.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            material: materials.add(Color::rgba_linear(0.0, 0.0, 0.0, 0.5).into()),
            ..Default::default()
        }
    ).insert(
        LeftBlackBar
    );

    commands.spawn_bundle(TextBundle {
        style: Style {
            align_self: AlignSelf::FlexStart,
            position_type: PositionType::Absolute,
            position: Rect {
                top: Val::Px(25.0),
                left: Val::Px(25.0),
                ..Default::default()
            },
            ..Default::default()
        },
        text: Text::with_section(
            "No finishers yet",
            TextStyle {
                font_size: 25.0,
                color: Color::WHITE,
                font: font.clone(),
            },
            TextAlignment {
                horizontal: HorizontalAlign::Left,
                ..Default::default()
            },
        ),
        ..Default::default()
    }).insert(
        Ranking { length: 0 }
    );
}

fn update_ranking_system(
    mut query: Query<(&mut Text, &mut Ranking)>,
    mut bar_query: Query<&mut Style, With<LeftBlackBar>>,
    ranking: Res<WinnersList>,
) {
    let (mut text, mut ranking_info) = query.single_mut().unwrap();

    if ranking_info.length != ranking.ranking.len() {
        text.sections[0].value = ranking.ranking.iter().enumerate().map(
            |(index, position)| format!("#{:} {:}\n", index + 1, position)
        ).collect();


        let mut bar = bar_query.single_mut().unwrap();
        bar.position.left = Val::Px(0.0);

        ranking_info.length = ranking.ranking.len();
    }
    // let font = text.sections[0].style.font.clone();
    // if text.sections.len() >= ranking.ranking.len() {
    //     for i in 0..ranking.ranking.len() {
    //         text.sections[i].value = format!("#{:} {:}", i + 1, ranking.ranking[i])
    //     }
    // } else {
    //     text.sections = ranking.ranking.iter().map(
    //         |i| TextSection {
    //             value: format!("#{:} {:}", 1, i),
    //             style: TextStyle {
    //                 font: font.clone(),
    //                 color: Color::WHITE,
    //                 font_size: 25.0
    //             },
    //         }
    //     ).collect()
    // }
}