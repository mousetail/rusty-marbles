use bevy::prelude::*;
use crate::AppState;

#[derive(Debug, Clone)]
pub struct KillfeedItem {
    pub(crate) name: String,
    pub(crate) time: f64,
}

#[derive(Default, Debug)]
pub struct Killfeed {
    pub(crate) feed: Vec<KillfeedItem>,
    offset: usize,
    offset_frac: f32,
}

pub struct KillfeedText;

pub struct KillfeedPlugin;

struct RightBlackBar;

impl Plugin for KillfeedPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app
            .add_system_set(
                SystemSet::on_enter(AppState::InGame).with_system(setup_killfeed_system.system())
            )
            .add_system_set(
                SystemSet::on_update(AppState::InGame)
                    .with_system(update_killfeed_system.system())
                    .with_system(update_rightbar_system.system())
            )
            .insert_resource(
                Killfeed {
                    ..Default::default()
                }
            )
        ;
    }
}

fn setup_killfeed_system(mut commands: Commands,
                         asset_server: Res<AssetServer>,
                         mut materials: ResMut<Assets<ColorMaterial>>) {
    commands.spawn_bundle(
        NodeBundle {
            style: Style {
                size: Size::new(Val::Percent(20.0), Val::Percent(100.0)),
                position_type: PositionType::Absolute,
                position: Rect {
                    right: Val::Percent(
                        -20.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            material: materials.add(Color::rgba_linear(0.0, 0.0, 0.0, 0.5).into()),
            ..Default::default()
        }
    ).insert(
        RightBlackBar
    );

    for _ in 0..20 {
        commands
            .spawn_bundle(TextBundle {
                style: Style {
                    align_self: AlignSelf::FlexEnd,
                    position_type: PositionType::Absolute,
                    position: Rect {
                        top: Val::Px(5.0),
                        right: Val::Px(5.0),
                        ..Default::default()
                    },
                    ..Default::default()
                },
                // Use the `Text::with_section` constructor
                text: Text::with_section(
                    // Accepts a `String` or any type that converts into a `String`, such as `&str`
                    "hello\nbevy!",
                    TextStyle {
                        font: asset_server.load("fonts/roboto.ttf"),
                        font_size: 33.0,
                        color: Color::WHITE,
                    },
                    // Note: You can use `Default::default()` in place of the `TextAlignment`
                    TextAlignment {
                        horizontal: HorizontalAlign::Center,
                        ..Default::default()
                    },
                ),
                ..Default::default()
            }).insert(
            KillfeedText {}
        );
    }
}

fn lerp_color(c1: Color, c2: Color, t: f32) -> Color {
    let c1_linear = c1.as_linear_rgba_f32();
    let c2_linear = c2.as_linear_rgba_f32();
    let mut color_lerped = [0.0; 4];
    for i in 0..4 {
        color_lerped[i] = c1_linear[i] * (1.0 - t) + c2_linear[i] * t
    }
    return Color::rgba_linear(
        color_lerped[0],
        color_lerped[1],
        color_lerped[2],
        color_lerped[3],
    );
}

fn update_rightbar_system(feed: Res<Killfeed>,
                          mut right_bar_query: Query<&mut Style, With<RightBlackBar>>,
) {
    let nrof_items = feed.feed.len();
    let mut right_bar = right_bar_query.single_mut().unwrap();
    if nrof_items == 0 {
        right_bar.position.right = Val::Percent(-20.0);
    } else {
        right_bar.position.right = Val::Px(0.0);
    }
}

fn update_killfeed_system(mut feed: ResMut<Killfeed>,
                          mut text_items: Query<(&mut Text, &mut Style), With<KillfeedText>>,
                          time: Res<Time>,
) {
    let nrof_items = feed.feed.len();
    let line_height = 33.0;

    let mut to_remove: Vec<usize> = vec![];
    let current_time = time.seconds_since_startup();

    let offset = -((nrof_items.min(20) + feed.offset) as f32) * line_height + feed.offset_frac;
    for (index, (item, (mut text, mut style))) in feed.feed.iter().rev().zip(text_items.iter_mut()).enumerate() {
        text.sections[0].value = item.name.clone();
        style.position.top = Val::Px(
            index as f32 * line_height +
                if index == 0 { 0.0 } else if index == 1 {
                    offset * 0.5
                } else { offset }); //Val::Px((nrof_items - index) as f32 * 55.0)

        let age = current_time - item.time;
        text.sections[0].style.color = if age < 2.5 { Color::RED } else if age < 3.0
        {
            lerp_color(Color::RED, Color::WHITE, (age as f32 - 2.5) * 2.0)
        } else if age < 9.5 { Color::WHITE } else {
            lerp_color(Color::WHITE, Color::rgba_linear(1.0, 1.0, 1.0, 0.0), (age as f32 - 9.5) * 2.0)
        };

        if age > 10.0 {
            to_remove.push(nrof_items - index - 1);
        }
    }

    for (mut text, mut _style) in text_items.iter_mut().skip(nrof_items) {
        text.sections[0].value = "".to_string();
    }

    if offset < 0.0 {
        feed.offset_frac += 4.0;
    }

    feed.offset += to_remove.len();
    for remove_index in to_remove {
        feed.feed.remove(remove_index);
    }


    feed.feed.truncate(20);
}