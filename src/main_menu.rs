use bevy::app::AppBuilder;
use bevy::input::ElementState;
use bevy::input::mouse::MouseButtonInput;
use bevy::prelude::*;
use crate::{AppState, NAMES, SystemSet};
use crate::VerticalAlign::Bottom;

pub struct MainMenuPlugin;

pub struct MainMenuItem;

const MARBLE_CELL_WIDTH: f32 = 225.0;
const MARBLE_CELL_MARGIN: f32 = 5.0;

impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system_set(
            SystemSet::on_enter(AppState::MainMenu)
                .with_system(
                    setup_main_menu.system()
                )
        ).add_system_set(
            SystemSet::on_update(AppState::MainMenu)
                .with_system(
                    update_main_menu.system()
                )
        ).add_system_set(
            SystemSet::on_exit(
                AppState::MainMenu
            ).with_system(
                remove_main_menu.system()
            )
        );
    }
}

fn truncate(s: &str, max_chars: usize) -> &str {
    match s.char_indices().nth(max_chars) {
        None => s,
        Some((idx, _)) => &s[..idx],
    }
}

struct StartButton;


fn setup_main_menu(mut commands: Commands, asset_server: Res<AssetServer>, window: Res<Windows>,
                   mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let font = asset_server.load("fonts/roboto.ttf");

    let screen = window.get_primary().unwrap();
    commands.spawn_bundle(
        TextBundle {
            style: Style {
                align_self: AlignSelf::Center,
                position_type: PositionType::Absolute,
                position: Rect {
                    top: Val::Px(25.0),
                    left: Val::Px(screen.width() as f32 / 2.0 - 200.0),
                    ..Default::default()
                },
                size: Size::new(Val::Px(400.0),
                                Val::Px(20.0)),

                ..Default::default()
            },
            text: Text::with_section(
                format!("Currently {} marbles playing", NAMES.len()),
                TextStyle {
                    font: font.clone(),
                    font_size: 35.0,
                    color: Color::WHITE,
                },
                // Note: You can use `Default::default()` in place of the `TextAlignment`
                TextAlignment {
                    horizontal: HorizontalAlign::Center,
                    ..Default::default()
                },
            ),
            ..Default::default()
        }
    ).insert(
        MainMenuItem
    );

    let width = (screen.width() as f32 / MARBLE_CELL_WIDTH) as usize;
    println!("Width {:}, steps: {:}", screen.width(), width);

    for (index, name) in NAMES.iter().enumerate() {
        let name_slice = truncate(name, 16);

        commands.spawn_bundle(
            NodeBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    position: Rect {
                        left: Val::Px(MARBLE_CELL_MARGIN +
                            (screen.width() as f32) / 2.0
                            - (MARBLE_CELL_WIDTH / 2.0) * (width as f32)
                            + MARBLE_CELL_WIDTH * (index % width) as f32),
                        top: Val::Px(55.0 + 40.0 * (index / width) as f32),
                        ..Default::default()
                    },
                    align_content: AlignContent::Center,
                    align_items: AlignItems::Center,
                    size: Size::new(
                        Val::Px(MARBLE_CELL_WIDTH - MARBLE_CELL_MARGIN * 2.0),
                        Val::Px(30.0),
                    ),
                    ..Default::default()
                },

                material: materials.add(Color::rgba_linear(0.5, 0.0, 0.0, 0.5).into()),
                ..Default::default()
            }
        ).insert(
            MainMenuItem
        ).with_children(|parent| {
            parent.spawn_bundle(
                TextBundle {
                    style: Style {
                        align_self: AlignSelf::Center,
                        position: Rect {
                            left: Val::Px(MARBLE_CELL_MARGIN),
                            ..Default::default()
                        },
                        ..Default::default()
                    },
                    text: Text::with_section(
                        name_slice,
                        TextStyle {
                            font: font.clone(),
                            font_size: 25.0,
                            color: Color::WHITE,
                        },
                        // Note: You can use `Default::default()` in place of the `TextAlignment`
                        TextAlignment {
                            horizontal: HorizontalAlign::Center,
                            ..Default::default()
                        },
                    ),
                    ..Default::default()
                }
            );
        });
    };

    // Start Button

    commands.spawn_bundle(ButtonBundle {
        style: Style {
            size: Size::new(Val::Px(MARBLE_CELL_WIDTH * 2.0), Val::Px(3.0 * 35.0)),
            margin: Rect::all(Val::Px(5.0)),

            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            position: Rect {
                bottom: Val::Px(15.0),
                left: Val::Px(screen.width() / 2.0 - MARBLE_CELL_WIDTH),
                ..Default::default()
            },
            ..Default::default()
        },
        material: materials.add(
            Color::rgba(0.0, 0.5, 0.0, 0.5).into()
        ),
        ..Default::default()
    }).insert(
        StartButton
    ).insert(
        MainMenuItem
    ).with_children(
        |parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    "Start Marble Run",
                    TextStyle {
                        font,
                        font_size: 30.0,
                        color: Color::rgb(0.9, 0.9, 0.9),
                    },
                    Default::default(),
                ),
                ..Default::default()
            });
        }
    );
}

fn update_main_menu(mut interaction_query: Query<
    (&Interaction, &mut Handle<ColorMaterial>),
    (Changed<Interaction>, With<StartButton>),
>,
                    mut materials: ResMut<Assets<ColorMaterial>>,
                    mut events: EventReader<MouseButtonInput>,
                    mut app_state: ResMut<State<AppState>>,
) {
    for (interaction, mut color) in interaction_query.iter_mut() {
        match *interaction {
            Interaction::Clicked => {
                *color = materials.add(Color::rgba(0.0, 0.6, 0.0, 0.5).into());
            }
            Interaction::Hovered => {
                *color = materials.add(Color::rgba(0.0, 0.7, 0.0, 0.5).into());
            }
            Interaction::None => {
                *color = materials.add(Color::rgba(0.0, 0.5, 0.0, 0.5).into());
            }
        }
    }

    for ev in events.iter() {
        match (ev.button, ev.state) {
            (MouseButton::Left, ElementState::Pressed) => {
                app_state.set(AppState::InGame).unwrap();
            }
            _ => ()
        }
    }
}


fn remove_main_menu(query: Query<Entity, With<MainMenuItem>>, mut commands: Commands) {
    for item in query.iter() {
        commands.entity(item).despawn_recursive();
    }
}